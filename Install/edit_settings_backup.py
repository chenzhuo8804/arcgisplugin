import Tkinter as tk
import os
import functools
import collections
import time
import copy
import ttk
from collections import OrderedDict
import json
import utils


cwd = os.path.dirname(os.path.abspath(__file__))
if 'PROJECT_ROOT' not in os.environ:
    os.environ.setdefault('PROJECT_ROOT', cwd)

project_root = os.environ['PROJECT_ROOT']


class memoized(object):

    '''Decorator. Caches a function's return value each time it is called.
    If called later with the same arguments, the cached value is returned
    (not reevaluated).
    '''

    def __init__(self, func):
        self.func = func
        self.cache = {}

        def __call__(self, *args):
            if not isinstance(args, collections.Hashable):
                # uncacheable. a list, for instance.
                # better to not cache than blow up.
                return self.func(*args)
            if args in self.cache:
                return self.cache[args]
            else:
                value = self.func(*args)
                self.cache[args] = value
                return value

            def __repr__(self):
                '''Return the function's docstring.'''
                return self.func.__doc__

            def __get__(self, obj, objtype):
                '''Support instance methods.'''
                return functools.partial(self.__call__, obj)


class cached_property(object):

    '''Decorator for read-only properties evaluated only once within TTL period.

    It can be used to created a cached property like this::

        import random

        # the class containing the property must be a new-style class
        class MyClass(object):
            # create property whose value is cached for ten minutes
            @cached_property(ttl=600)
            def randint(self):
                # will only be evaluated every 10 min. at maximum.
                return random.randint(0, 100)

    The value is cached  in the '_cache' attribute of the object instance that
    has the property getter method wrapped by this decorator. The '_cache'
    attribute value is a dictionary which has a key for every property of the
    object which is wrapped by this decorator. Each entry in the cache is
    created only when the property is accessed for the first time and is a
    two-element tuple with the last computed property value and the last time
    it was updated in seconds since the epoch.

    The default time-to-live (TTL) is 300 seconds (5 minutes). Set the TTL to
    zero for the cached value to never expire.

    To expire a cached property value manually just do::

        del instance._cache[<property name>]

    '''

    def __init__(self, ttl=300):
        self.ttl = ttl

    def __call__(self, fget, doc=None):
        self.fget = fget
        self.__doc__ = doc or fget.__doc__
        self.__name__ = fget.__name__
        self.__module__ = fget.__module__
        return self

    def __get__(self, inst, owner):
        now = time.time()
        try:
            value, last_update = inst._cache[self.__name__]
            if self.ttl > 0 and now - last_update > self.ttl:
                raise AttributeError
        except (KeyError, AttributeError):
            value = self.fget(inst)
            try:
                cache = inst._cache
            except AttributeError:
                cache = inst._cache = {}
            cache[self.__name__] = (value, now)
        return value


class ToggledFrame(tk.Frame):
    '''A frame with toggle button on top'''
    def __init__(self, parent, text="", del_button=False,
                 add_button=False, add_func=None, *args, **options):
        tk.Frame.__init__(self, parent, *args, **options)
        self.show = tk.IntVar()
        self.show.set(0)

        self.title_frame = ttk.Frame(self)
        self.title_frame.pack(fill="x")

        self.title_label = ttk.Label(self.title_frame, text=text)
        self.title_label.pack(side="left", fill="x", expand=1)

        if del_button:
            self.del_button = ttk.Button(self.title_frame, width=2, text='-',
                                         style='Toolbutton')
            self.del_button.pack(side='left')

        if add_button:
            self.add_button = ttk.Button(self.title_frame, width=4, text='Add',
                                         style='Toolbutton')
            self.add_button.pack(side='left')

        self.toggle_button = ttk.Checkbutton(
            self.title_frame, width=2, text='+', command=self.toggle,
            variable=self.show, style='Toolbutton')
        self.toggle_button.pack(side="left")
        self._expanded = False
        self.sub_frame = tk.Frame(self, relief="sunken", borderwidth=1)

    def add(self):
        raise NotImplementedError

    def remove(self):
        print('remove')

    def modify_title(self, text):
        self.title_label.configure(text=text)

    def toggle(self):
        if bool(self.show.get()):
            self._expanded = True
            self.sub_frame.pack(fill="x", expand=1)
            self.toggle_button.configure(text='-')
        else:
            self._expanded = False
            self.sub_frame.forget()
            self.toggle_button.configure(text='+')
        return

    def toggle_on(self):
        if not self._expanded:
            self._expanded = True
            self.sub_frame.pack(fill='x', expand=1)
            self.toggle_button.configure(text='-')
        return


class KeyValueType:
    LabelEntry = 1
    EntryEntry = 2
    LabelCombobox = 3


class KeyValueEntryCellFrame(tk.Frame):

    def __init__(self, parent, key='', value='', choice=[],
                 entity_type=KeyValueType.LabelEntry, *args, **kwargs):
        tk.Frame.__init__(self, parent, *args, **kwargs)
        self._entries = []
        # Key area
        if entity_type in (KeyValueType.LabelEntry,
                           KeyValueType.LabelCombobox):
            self._label = tk.Label(self, text=key, borderwidth=0, width=20,
                                   padx=5, pady=5, anchor=tk.W)
            self._label.grid(row=0, column=0)
            self._entries.append(self._label)

        elif entity_type == KeyValueType.EntryEntry:
            kv = tk.StringVar()
            self._keyentry = tk.Entry(self, textvariable=kv, width=20)
            kv.set(key)
            self._entries.append(self._keyentry)

        # Value area
        if entity_type in (KeyValueType.LabelEntry,
                           KeyValueType.EntryEntry):
            vv = tk.StringVar()
            self._ventry = tk.Entry(self, textvariable=vv, width=30)
            vv.set(value)
            self._entries.append(self._ventry)

        elif entity_type == KeyValueType.LabelCombobox:
            self._vcombo = tk.StringVar()
            self._vcombo.set(value)
            self._vcbbox = ttk.Combobox(
                self, width=27, textvariable=self._vcombo)
            self._vcbbox['values'] = choice
            self._entries.append(self._vcbbox)

        self._entries[0].grid(row=0, column=0)
        self._entries[1].grid(row=0, column=1)

    @property
    def value(self):
        value_dict = {}
        k = self._entries[0].cget('text') if isinstance(self._entries[0], tk.Label) \
            else self._entries[0].get()
        v = self._entries[1].get()
        value_dict[k] = v
        return value_dict


class KeyValueEntryPHFrame(ToggledFrame):

    ''' For dictionaries'''

    def __init__(self, parent, title, fields=[], *args, **kwargs):
        ToggledFrame.__init__(self, parent, title, *args, **kwargs)
        self._widgets = []
        self._entries = {}

        # frame for label and entries
        self._entries_frame = ttk.Frame(self.sub_frame)
        self._entries_frame.pack(fill='x')

        # frame for other frames(dict)
        self.dict_frame = ttk.Frame(self.sub_frame)
        self.dict_frame.pack(fill='x')

        self._fields = copy.deepcopy(fields)

        def is_entity_entry(row):
            return isinstance(row[1], list) and isinstance(row[1][0], tuple)

        def is_label_combox(row):
            # third argument is a list
            return len(row) >= 3 and isinstance(row[2], list)

        def is_label_entry(row):
            # using else
            pass

        # if field has been set here
        if fields:
            for row_index in xrange(len(fields)):
                if is_entity_entry(fields[row_index]):
                    sub_frame = KeyValueEntryPHFrame(self.dict_frame,
                                                     fields[row_index][0],
                                                     fields[row_index][1],
                                                     relief='raised',
                                                     borderwidth=1)
                    sub_frame.pack(
                        fill='x', expand=1, pady=2, padx=2, anchor='n')
                    self._widgets.append(sub_frame)

                elif is_label_combox(fields[row_index]):
                    frame = KeyValueEntryCellFrame(self._entries_frame,
                                                   entity_type=KeyValueType.LabelCombobox,
                                                   key=fields[row_index][0],
                                                   value=fields[row_index][1],
                                                   choice=fields[row_index][2],
                                                   borderwidth=0, width=20,
                                                   padx=5, pady=5)
                    frame.pack()
                    self._widgets.append(frame)

                else:
                    frame = KeyValueEntryCellFrame(self._entries_frame,
                                                   entity_type=KeyValueType.LabelEntry,
                                                   key=fields[row_index][0],
                                                   value=fields[row_index][1],
                                                   borderwidth=0, width=20,
                                                   padx=5, pady=5)
                    frame.pack()
                    self._widgets.append(frame)

                # elif is_label_combox(fields[row_index]):
                #     label = tk.Label(self._entries_frame,
                #                      text=fields[row_index][0],
                #                      borderwidth=0, width=20, padx=5,
                #                      pady=5, anchor=tk.W)
                #     label.grid(row=row_index, column=0, sticky=tk.W)
                #     cbbox = ttk.Combobox(self._entries_frame,
                #                          width=30)
                #     cbbox['values'] = fields[row_index][1]
                #     cbbox.current(0)
                #     cbbox.grid(row=row_index, column=1)

                # else:
                #     label = tk.Label(self._entries_frame,
                #                      text=fields[row_index][0],
                #                      borderwidth=0, width=20, padx=5,
                #                      pady=5, anchor=tk.W)
                #     label.grid(row=row_index, column=0, sticky=tk.W)
                #     v = tk.StringVar()
                #     entry = tk.Entry(
                #         self._entries_frame, textvariable=v, width=30)
                #     entry.grid(row=row_index, column=1, sticky='ew')
                #     v.set(fields[row_index][1])

                #     self._entries[fields[row_index][0]] = entry
                #     self._widgets.append(('label_entry', label, entry))

    def set(self, row, value):
        widget = self._widgets[row][0]
        widget.configure(text=value)

    @property
    def fields(self):
        return self._fields

    @cached_property()
    def entries(self):
        return {k: self._entries[k].get() for k in self._entries}

    @property
    def value(self):
        value_dict = {}
        for v in self._widgets:
            value_dict.update(v.value)

        return value_dict


class KeyValueEntryDynamicPHFrame(KeyValueEntryPHFrame):

    '''
    Placeholder for entity entries, which allows user to add entries
    '''

    def __init__(self, parent, title, fields=[], add_button=True, *args, **kwargs):
        KeyValueEntryPHFrame.__init__(self, parent, title, fields=fields,
                                      add_button=add_button, *args, **kwargs)
        self.add_button.configure(command=self.add)

    def add(self, key='', value=''):
        frame = KeyValueEntryCellFrame(self._entries_frame,
                                       entity_type=KeyValueType.EntryEntry,
                                       key=key, value=value)
        frame.pack()
        self._widgets.append(frame)
        self.toggle_on()


class ArrayCellFrame(KeyValueEntryPHFrame):

    '''
    Placeholders for array cells
    '''

    def __init__(self, parent, placeholder, title='', fields=[], *args, **kwargs):
        KeyValueEntryPHFrame.__init__(
            self, parent, title=title, fields=fields,
            del_button=True, *args, **kwargs)
        self._title = title
        self._placeholder = placeholder
        self.del_button.configure(command=self.remove)

    def remove(self):
        self._placeholder.remove(int(self._title))


class ArrayPHFrame(ToggledFrame):

    '''
    Placeholders for array items
    '''

    def __init__(self, parent, text='', fields=[], *args, **kwargs):
        ToggledFrame.__init__(self, parent, text=text, add_button=True,
                              *args, **kwargs)
        # fields for the array
        self._fields = copy.deepcopy(fields)
        self._frames = []
        self.add_button.configure(command=self.add)
        for f in fields:
            self.add(fields=f)

    def add(self, fields=None):
        if fields is None:
            fields = self._fields[0]
        array_cell = ArrayCellFrame(self.sub_frame, self,
                                    (len(self._frames)), fields=fields,
                                    relief='raised', borderwidth=1)
        self._frames.append(array_cell)
        array_cell.pack(fill='x', expand=1, pady=2, padx=2, anchor='n')
        self.toggle_on()

    def remove(self, index):
        self._frames[index].forget()
        del self._frames[index]
        for i in xrange(len(self._frames)):
            self._frames[i].modify_title(str(i))

    @property
    def value(self):
        return [v.value for v in self._frames]


class InputAreaPH(tk.Frame):

    '''
    Place holder for no toggle
    '''

    def __init__(self, parent, fields, *args, **kwargs):
        tk.Frame.__init__(
            self, parent, padx=5, pady=5, *args, **kwargs)
        self.frame = tk.Frame(self)
        self.frame.pack()
        self._widgets = []
        self._entries = {}
        if isinstance(fields, list):
            fields_iter = fields
        elif isinstance(fields, dict):
            fields_iter = fields.iteritems()

        for key, value in fields_iter:
            frame = KeyValueEntryCellFrame(self.frame, key, value,
                                           relief='raised', borderwidth=1)
            frame.pack(fill='x', expand=1, pady=2, padx=2, anchor='n')

            self._widgets.append(frame)

    def set(self, row, value):
        widget = self._widgets[row][0]
        widget.configure(text=value)

    @cached_property()
    def entries(self):
        res = {k: self._entries[k].get() for k in self._entries}
        return res

    @property
    def value(self):
        # Only key, entry is found here
        value_dict = {}
        for frame in self._widgets:
            value_dict.update(frame.value)

        return value_dict


class ScrollFrame(tk.Frame):

    def __init__(self, root):
        tk.Frame.__init__(self, root)
        self.canvas = tk.Canvas(root, borderwidth=0)
        self.frame = tk.Frame(self.canvas)
        self.vsb = tk.Scrollbar(
            root, orient="vertical", command=self.canvas.yview)
        self.canvas.configure(yscrollcommand=self.vsb.set)

        self.vsb.pack(side="right", fill="y")
        self.canvas.pack(side="left", fill="both", expand=True)
        self.canvas.create_window((4, 4), window=self.frame, anchor="nw",
                                  tags="self.frame")

        self.frame.bind("<Configure>", self.onFrameConfigure)

    def onFrameConfigure(self, event):
        '''Reset the scroll region to encompass the inner frame'''
        self.canvas.configure(scrollregion=self.canvas.bbox("all"))


def save():
    values = properties.value
    values['maxReadings'] = int(values['maxReadings'])

    settings = utils.project_setting(project_root)
    settings.update(values)
    utils.set_project_setting(settings, project_root)
    save_status_label.pack()
    # post_body = {}
    # post_body['description'] = description.value
    # post_body['metadata'] = copy.deepcopy(metadata.value)
    # post_body['Locations'] = [copy.deepcopy(locations.value)]
    # post_body['Datastreams'] = [copy.deepcopy(datastreams.value)]
    pass

if __name__ == '__main__':
    'python .py create base-url'
    'python .py edit baseUrl id'

    root = tk.Tk()
    root.wm_title("Create a thing")

    main_frame = tk.Frame(root)
    main_frame.pack(side='top', fill='both', expand=True)

    # Construct values from json

    setting_json = utils.project_setting(project_root)
    setting_keys = ['maxReadings', 'defaultUrl', 'layerFile']
      # Add stuff

    settings = []
    for k in setting_keys:
        v = setting_json[k]
        settings.append((k, v))

    properties = KeyValueEntryPHFrame(main_frame, 'Settings', settings,
                                      relief='raised', borderwidth=1)

    properties.pack(fill='x', expand=1, pady=2, padx=2, anchor='n')
    properties.toggle_on()

    save_status_frame = ttk.Frame(root)
    save_status_frame.pack()
    save_status_label = ttk.Label(save_status_frame, text='Successful')
    save_button = ttk.Button(root, text='Save', command=save)
    save_button.pack()
    tk.mainloop()
