"""Provided file manipulating functions, you can use functions provided
here to load/save project settings"""
import os
import json
import crypto
import utils

from datetime import datetime

# TODO: Setting should be a global varialbe, learn from Django
def project_setting(root_folder):
    """Get project setting files, if file not available, create a default one"""
    file_name = os.path.join(root_folder, 'setting.geocens')

    # If no setting is found, create a new one
    if not os.path.exists(file_name):
        # status and username are not used if loggin is not used
        setting_dict = set_project_setting(
            {'status': utils.STATUS_NOT_LOGGEDIN,
             'dataStatus': utils.DATASTATUS_NOTREADY,
             'data': '',
             "layerFile": "thingsTimeSeries.lyr",
             "maxReadings": 500,
             "timeout": 1,
             "keysDict": {
                 "Datastream": "Datastream",
                 "FeatureOfInterest": "FeatureOfInterest",
                 "Things": "Things",
                 "description": "description",
                 "id": "@iot.id",
                 "location": "location",
                 "nextLink": "nextLink",
                 "phenomenonTime": "phenomenonTime",
                 "properties": "properties",
                 "result": "result",
                 "selfLink": "@iot.selfLink",
                 "value": "value"
             }}, root_folder)

        return setting_dict

    with open(file_name, 'r') as fobj:
        content_json = crypto.decrypt(fobj.read())
        return json.loads(content_json)


def set_project_setting(setting_dict, root_foler):
    """Set project setting files"""
    file_name = os.path.join(root_foler, 'setting.geocens')

    setting_dict['lastUpdate'] = datetime.utcnow().isoformat()
    content_text = crypto.encrypt(
        json.dumps(setting_dict, indent=2, separators=(',', ': ')))
    with open(file_name, 'w') as fobj:
        fobj.write(content_text)
    return setting_dict
