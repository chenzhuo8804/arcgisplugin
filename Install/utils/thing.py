"""Thing class

This module provided thing class, which is an abstraction of sensor things
"""
import datetime
import json
import requests
import os
import sys
import copy
import urllib

from . import urls

# const strings

cwd = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
if 'PROJECT_ROOT' not in os.environ:
    os.environ.setdefault('PROJECT_ROOT', cwd)

project_root = os.environ['PROJECT_ROOT']

STATUS_NOT_LOGGEDIN = 'not logged in'
STATUS_LOGGEDIN = 'logged in'
DATASTATUS_NOTREADY = 'not ready'
DATASTATUS_DOWNLOADING = 'downloading'
DATASTATUS_READY = 'ready'


def from_utc(utc_time, fmt="%Y-%m-%dT%H:%M:%S.%f"):

    # change datetime.datetime to time, return time.struct_time type
    if utc_time.endswith('Z'):
        fmt += 'Z'
    try:
        dtime = datetime.datetime.strptime(utc_time, fmt)
        return dtime

    except Exception:
        fmt = "%Y-%m-%dT%H:%M:%S"
        dtime = datetime.datetime.strptime(utc_time, fmt)
        return dtime

class ThingEncoder(json.JSONEncoder):

    """JSON Encoder for a thing"""

    def default(self, obj):

        # TODO: probably it is not safe, Thing is unknown to this module yet
        # Probably let user provide its own Enc/Decoder?
        if isinstance(obj, Thing):
            return {'description': obj.desc,
                    'properties': obj.prop,
                    'Locations': [obj.Location],
                    'coordinates': obj.coord,
                    'geomType': obj.geom_type,
                    'Datastreams': obj.datastream,
                    'id': obj.thing_id}

        elif isinstance(obj, datetime.datetime):
            return obj.isoformat()


class ThingDecoder(json.JSONDecoder):

    """JSON Decoder for a thing"""

    def decode(self, obj):
        if isinstance(obj, datetime.datetime):
            return from_utc(obj)

        return super(ThingDecoder, self).decode(obj)


class Thing(object):

    '''Representation of a sensor thing'''

    def __init__(self, desc, loc, thing_id, base_url='', thing_dir='',
                 properties={}, proj_setting={}):
        super(Thing, self).__init__()
        self.base_url = base_url
        thing_params = {'$expand': 'Datastreams'}
        self.thing_url = urls.thing_endpoint(
            self.base_url, thing_id) + '?' + urllib.urlencode(thing_params)

        # construct thing_url
        self.desc = desc
        self.prop = properties
        self.proj_setting = proj_setting
        self.keys_dict = self.proj_setting['keysDict']
        self.Location = loc
        if self.keys_dict['location'] in loc:
            self.location = loc[self.keys_dict['location']]
        else:
            self.location = loc['feature']

        self.coord = self.location['coordinates']
        self.geom_type = self.location['type']
        self.thing_id = thing_id
        self.datastream = {}
        self.thing_dir = thing_dir
        self.obsv_max = - sys.maxint - 1
        self.obsv_min = sys.maxint

    def add_ds_old(self, ds):
        '''Adding a datastream to current thing'''
        if ds[self.keys_dict['id']] not in self.datastream:
            self.datastream[ds[self.keys_dict['id']]] = copy.copy(ds)
            self.datastream[ds[self.keys_dict['id']]]['Observations'] = []
            self.datastream[ds[self.keys_dict['id']]]['min'] = sys.maxint
            self.datastream[ds[self.keys_dict['id']]]['max'] = - sys.maxint - 1

        return

    def add_ds_with_obsv(self, ds):
        '''
        Adding a datastream along with its observations to current thing
        In most cases when you call Datastream(id) endpoint with $expand=Observations
        options, you will want to call this function to add datastream and observations
        '''
        ds_valid_keys = [self.keys_dict[i] if i in self.keys_dict else i for i in [
            'unitOfMeasurement', 'observationType', 'id', 'description']]
        ds_id = ds[self.keys_dict['id']]

        # create new datastream and assign it
        ds_new = {i: ds[i] for i in ds_valid_keys}
        if ds[self.keys_dict['id']] not in self.datastream:
            self.datastream[ds_id] = ds_new

        self.datastream[ds_id]['Observations'] = []
        self.datastream[ds_id]['min'] = sys.maxint
        self.datastream[ds_id]['max'] = - sys.maxint - 1
        # process observations and save it to local

        obsv_raw = ds['Observations']
        for item in obsv_raw:
            self.add_obss(ds_id, {'result': item['result'],
                                  'phenomenonTime': item['phenomenonTime']})

    def add_obss(self, ds_id, obsv):
        '''Adding an observation to current thing, updating min/max value'''
        # In case people forgot to call add_ds
        # update min/max
        self.datastream[ds_id]['Observations'].append(obsv)

        # TODO: Observation will always be numbers?
        try:
            if float(obsv['result']) < self.datastream[ds_id]['min']:
                self.datastream[ds_id]['min'] = float(obsv['result'])
            if float(obsv['result']) > self.datastream[ds_id]['max']:
                self.datastream[ds_id]['max'] = float(obsv['result'])
        except Exception as e:
            # Some observations are not float, dont show them
            pass

    def init_obss(self):
        '''Init datastreams and observations for current thing
        After getting a sensor thing from Thing Endpoint, you need to call this function
        to initiate current thing'''
        # print(self.url_ds.substitute(thing_id=self.thing_id))
        thing_resp_raw = requests.get(self.thing_url, timeout=1)
        # TODO: check if thing is valid
        thing_resp_dict = thing_resp_raw.json()
        if not self.prop:
            self.prop = thing_resp_dict[self.keys_dict['properties']]
        ds_params = {'$expand': 'ObservedProperty,Sensor,Observations'}
        for ds in thing_resp_dict['Datastreams']:
            ds_url_base = urls.datastream_endpoint(
                self.base_url, ds[self.keys_dict['id']])
            ds_url = ds_url_base + '?' + urllib.urlencode(ds_params)
            ds_resp_raw = requests.get(ds_url, timeout=1)
            # TODO: check if thing is valid
            ds_resp_dict = ds_resp_raw.json()
            self.add_ds_with_obsv(ds_resp_dict)
        # sort observations based on time

    def dumps(self):
        '''dump things to txt files'''

        self.thing_file_name = os.path.join(self.thing_dir,
                                            'thing_%s.txt' % self.thing_id)
        thing_text = json.dumps(
            self, cls=ThingEncoder, indent=2, separators=(',', ': '))
        with open(self.thing_file_name, 'w') as fobj:
            fobj.write(thing_text)
