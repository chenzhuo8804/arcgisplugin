# -*- coding: utf-8 -*-
"""Tkinter application for downloading GUIs

This module provides download gui for SensorUp ArcGIS Plugin
When user click 'Advanced' button in SensorUp plugin, ArcMap will call this file using Popen.
"""
import argparse
import requests
import threading
import utils
import shutil
import os
import urlparse
import urllib
import sys

import Tkinter as tk
import ttk
from datetime import datetime, timedelta

from utils import Thing
from utils import gui
from functools import partial
import json

from utils import urls

cwd = os.path.dirname(os.path.abspath(__file__))
if 'PROJECT_ROOT' not in os.environ:
    os.environ.setdefault('PROJECT_ROOT', cwd)

project_root = os.environ['PROJECT_ROOT']

# A default value
timeout = 100000

proj_settings = utils.project_setting(project_root)
keys_dict = proj_settings['keysDict']


class cached_property(object):

    '''Decorator for read-only properties evaluated only once within TTL period.

    It can be used to created a cached property like this::

        import random

        # the class containing the property must be a new-style class
        class MyClass(object):
            # create property whose value is cached for ten minutes
            @cached_property(ttl=600)
            def randint(self):
                # will only be evaluated every 10 min. at maximum.
                return random.randint(0, 100)

    The value is cached  in the '_cache' attribute of the object instance that
    has the property getter method wrapped by this decorator. The '_cache'
    attribute value is a dictionary which has a key for every property of the
    object which is wrapped by this decorator. Each entry in the cache is
    created only when the property is accessed for the first time and is a
    two-element tuple with the last computed property value and the last time
    it was updated in seconds since the epoch.

    The default time-to-live (TTL) is 300 seconds (5 minutes). Set the TTL to
    zero for the cached value to never expire.

    To expire a cached property value manually just do::

        del instance._cache[<property name>]

    '''

    def __init__(self, ttl=300):
        self.ttl = ttl

    def __call__(self, fget, doc=None):
        self.fget = fget
        self.__doc__ = doc or fget.__doc__
        self.__name__ = fget.__name__
        self.__module__ = fget.__module__
        return self

    def __get__(self, inst, owner):
        now = time.time()
        try:
            value, last_update = inst._cache[self.__name__]
            if self.ttl > 0 and now - last_update > self.ttl:
                raise AttributeError
        except (KeyError, AttributeError):
            value = self.fget(inst)
            try:
                cache = inst._cache
            except AttributeError:
                cache = inst._cache = {}
            cache[self.__name__] = (value, now)
        return value


class ToggledFrame(tk.Frame):

    '''A frame with toggle button on top'''

    def __init__(self, parent, text="", del_button=False,
                 add_button=False, add_func=None, *args, **options):
        tk.Frame.__init__(self, parent, *args, **options)
        print('add button {}'.format(add_button))
        self.show = tk.IntVar()
        self.show.set(0)

        self.title_frame = ttk.Frame(self)
        self.title_frame.pack(fill="x")

        self.title_label = ttk.Label(self.title_frame, text=text)
        self.title_label.pack(side="left", fill="x", expand=1)

        if del_button:
            self.del_button = ttk.Button(self.title_frame, width=2, text='-',
                                         style='Toolbutton')
            self.del_button.pack(side='left')

        if add_button:
            self.add_button = ttk.Button(self.title_frame, width=4, text='Add',
                                         style='Toolbutton')
            self.add_button.pack(side='left')

        self.toggle_button = ttk.Checkbutton(
            self.title_frame, width=2, text='+', command=self.toggle,
            variable=self.show, style='Toolbutton')
        self.toggle_button.pack(side="left")
        self._expanded = True
        self.sub_frame = tk.Frame(self, relief="sunken", borderwidth=1)

    def add(self):
        print(self)
        raise NotImplementedError

    def remove(self):
        print('remove')

    def modify_title(self, text):
        self.title_label.configure(text=text)

    def toggle(self):
        if bool(self.show.get()):
            self._expanded = True
            self.sub_frame.pack(fill="x", expand=1)
            self.toggle_button.configure(text='-')
        else:
            self._expanded = False
            self.sub_frame.forget()
            self.toggle_button.configure(text='+')
        return

    def toggle_on(self):
        if not self._expanded:
            self._expanded = True
            self.sub_frame.pack(fill='x', expand=1)
            self.toggle_button.configure(text='-')
        return


class KeyValueType:
    LabelEntry = 1
    EntryEntry = 2
    LabelCombobox = 3


class KeyValueEntryCellFrame(tk.Frame):

    def __init__(self, parent, key='', value='', choice=[],
                 entity_type=KeyValueType.LabelEntry, *args, **kwargs):
        tk.Frame.__init__(self, parent, *args, **kwargs)
        self._entries = []
        # Key area
        if entity_type in (KeyValueType.LabelEntry,
                           KeyValueType.LabelCombobox):
            self._label = tk.Label(self, text=key, borderwidth=0, width=20,
                                   padx=5, pady=5, anchor=tk.W)
            self._label.grid(row=0, column=0)
            self._entries.append(self._label)

        elif entity_type == KeyValueType.EntryEntry:
            kv = tk.StringVar()
            self._keyentry = tk.Entry(self, textvariable=kv, width=20)
            kv.set(key)
            self._entries.append(self._keyentry)

        # Value area
        if entity_type in (KeyValueType.LabelEntry,
                           KeyValueType.EntryEntry):
            vv = tk.StringVar()
            self._ventry = tk.Entry(self, textvariable=vv, width=30)
            vv.set(value)
            self._entries.append(self._ventry)

        elif entity_type == KeyValueType.LabelCombobox:
            self._vcombo = tk.StringVar()
            self._vcombo.set(value)
            self._vcbbox = ttk.Combobox(
                self, width=27, textvariable=self._vcombo)
            self._vcbbox['values'] = choice
            self._entries.append(self._vcbbox)

        self._entries[0].grid(row=0, column=0)
        self._entries[1].grid(row=0, column=1)

    @property
    def value(self):
        value_dict = {}
        k = self._entries[0].cget('text') if isinstance(self._entries[0], tk.Label) \
            else self._entries[0].get()
        v = self._entries[1].get()
        value_dict[k] = v
        return value_dict


class KeyValueEntryPHFrame(ToggledFrame):

    ''' For dictionaries'''

    def __init__(self, parent, title, fields=[], *args, **kwargs):
        ToggledFrame.__init__(self, parent, title, *args, **kwargs)
        self._widgets = []
        self._entries = {}

        # frame for label and entries
        self._entries_frame = ttk.Frame(self.sub_frame)
        self._entries_frame.pack(fill='x')

        # frame for other frames(dict)
        self.dict_frame = ttk.Frame(self.sub_frame)
        self.dict_frame.pack(fill='x')

        self._fields = copy.deepcopy(fields)

        def is_entity_entry(row):
            return isinstance(row[1], list) and isinstance(row[1][0], tuple)

        def is_label_combox(row):
            # third argument is a list
            return len(row) >= 3 and isinstance(row[2], list)

        def is_label_entry(row):
            # using else
            pass

        # if field has been set here
        if fields:
            for row_index in xrange(len(fields)):
                if is_entity_entry(fields[row_index]):
                    sub_frame = KeyValueEntryPHFrame(self.dict_frame,
                                                     fields[row_index][0],
                                                     fields[row_index][1],
                                                     relief='raised',
                                                     borderwidth=1)
                    sub_frame.pack(
                        fill='x', expand=1, pady=2, padx=2, anchor='n')
                    self._widgets.append(sub_frame)

                elif is_label_combox(fields[row_index]):
                    frame = KeyValueEntryCellFrame(self._entries_frame,
                                                   entity_type=KeyValueType.LabelCombobox,
                                                   key=fields[row_index][0],
                                                   value=fields[row_index][1],
                                                   choice=fields[row_index][2],
                                                   borderwidth=0, width=20,
                                                   padx=5, pady=5)
                    frame.pack()
                    self._widgets.append(frame)

                else:
                    print(fields[row_index][1])
                    frame = KeyValueEntryCellFrame(self._entries_frame,
                                                   entity_type=KeyValueType.LabelEntry,
                                                   key=fields[row_index][0],
                                                   value=fields[row_index][1],
                                                   borderwidth=0, width=20,
                                                   padx=5, pady=5)
                    frame.pack()
                    self._widgets.append(frame)

                # elif is_label_combox(fields[row_index]):
                #     label = tk.Label(self._entries_frame,
                #                      text=fields[row_index][0],
                #                      borderwidth=0, width=20, padx=5,
                #                      pady=5, anchor=tk.W)
                #     label.grid(row=row_index, column=0, sticky=tk.W)
                #     cbbox = ttk.Combobox(self._entries_frame,
                #                          width=30)
                #     cbbox['values'] = fields[row_index][1]
                #     cbbox.current(0)
                #     cbbox.grid(row=row_index, column=1)

                # else:
                #     label = tk.Label(self._entries_frame,
                #                      text=fields[row_index][0],
                #                      borderwidth=0, width=20, padx=5,
                #                      pady=5, anchor=tk.W)
                #     label.grid(row=row_index, column=0, sticky=tk.W)
                #     v = tk.StringVar()
                #     entry = tk.Entry(
                #         self._entries_frame, textvariable=v, width=30)
                #     entry.grid(row=row_index, column=1, sticky='ew')
                #     v.set(fields[row_index][1])

                #     self._entries[fields[row_index][0]] = entry
                #     self._widgets.append(('label_entry', label, entry))

    def set(self, row, value):
        widget = self._widgets[row][0]
        widget.configure(text=value)

    @property
    def fields(self):
        return self._fields

    @cached_property()
    def entries(self):
        return {k: self._entries[k].get() for k in self._entries}

    @property
    def value(self):
        value_dict = {}
        for v in self._widgets:
            value_dict.update(v.value)

        return value_dict


class KeyValueEntryDynamicPHFrame(KeyValueEntryPHFrame):

    '''
    PH for entity entries, which allows user to add entries
    '''

    def __init__(self, parent, title, fields=[], add_button=True, *args, **kwargs):
        KeyValueEntryPHFrame.__init__(self, parent, title, fields=fields,
                                      add_button=add_button, *args, **kwargs)
        self.add_button.configure(command=self.add)

    def add(self, key='', value=''):
        frame = KeyValueEntryCellFrame(self._entries_frame,
                                       entity_type=KeyValueType.EntryEntry,
                                       key=key, value=value)
        frame.pack()
        self._widgets.append(frame)
        self.toggle_on()


def from_location(root, base_url, max_readings, spatial_query, debug):
    # use a dict to manage things

    # Store stuff in this folder
    things_path = os.path.join(project_root, 'things')

    if os.path.exists(things_path):
        try:
            shutil.rmtree(things_path)
        except Exception as e:
            print('Failed to rm things folder, reason %s' % str(e))
            return

    os.mkdir(things_path)

    proj_settings = utils.project_setting(project_root)
    proj_settings['dataStatus'] = utils.DATASTATUS_DOWNLOADING
    proj_settings['thingObservations'] = {}
    utils.set_project_setting(proj_settings, project_root)

    progress_bar.configure(length=100)
    progress_bar.pack()
    progress_label.pack()

    things_list = []

    if debug:
        things_id_debug = []
        for thing_id in things_id_debug:
            params = {'$expand': 'Locations', '$filter': spatial_query}
            thing_url = urls.locations_endpoint(
                base_url) + '?' + urlparse.urlencode(params)
            # thing_url = r'%s/Location?$expand=Locations&$filter=%s' %
            # (base_url, spatial_query)
            thing_response = requests.get(thing_url).json()
            things_list.append(thing_response)

    else:
        params = {'$top': max_readings * 100,
                  '$filter': spatial_query,
                  '$expand': 'Things'}

        things_url = urls.locations_endpoint(
            base_url) + '?' + urllib.urlencode(params)
        print(things_url)
        locs_list = requests.get(things_url).json()['value']

        for index, loc in enumerate(locs_list):

            # Some observation has no things
            if loc[keys_dict['Things']]:
                thing_id = loc[keys_dict['Things']][0][keys_dict['id']]
                thing_location = {'location': loc[keys_dict['location']]}
                # TODO: consider using paralles to take care of them
                proj_settings = utils.project_setting(project_root)
                thing = Thing(desc=loc[keys_dict['Things']][0][keys_dict['description']],
                              # TODO: Only one locations?
                              loc=thing_location,
                              thing_id=thing_id,
                              base_url=base_url,
                              thing_dir=things_path,
                              properties=loc[keys_dict['Things']][
                                  0][keys_dict['properties']],
                              proj_setting=proj_settings)
                thing.init_obss()
                thing.dumps()

                proj_settings['thingObservations'][
                    thing_id] = thing.thing_file_name
                utils.set_project_setting(proj_settings, project_root)
                progress_label.configure(text='{} / {}'.format(index, len(locs_list)))
                progress_bar['value'] = index / float(len(locs_list)) * 100

    proj_settings = utils.project_setting(project_root)
    proj_settings['dataStatus'] = utils.DATASTATUS_READY
    utils.set_project_setting(proj_settings, project_root)
    root.destroy()
    # should have an update function?

    # things_local_dict = utils.things(project_root)
    # things_local_dict['things'] = things_dict
    # utils.set_things(project_root, things_local_dict)


def from_observation(root, url, max_readings, download_filter, debug):
    # so new obsvs can pick from existing dss
    # http://chashuhotpot.sensorup.com/OGCSensorThings/v1.0/Observations?$expand=FeatureOfInterest,Datastream
    ds_dict = {}
    thing_dict = {}
    nt_count = 0
    base_url = url
    params = {'$expand': 'FeatureOfInterest,Datastream'}
    progress_label.pack()
    progress_bar.pack()

    url += 'Observations?$expand=FeatureOfInterest,Datastream'
    if download_filter:
        if 'start' in download_filter:
            # filter by time option is enabled
            params['$top'] = 100
            params['$filter'] = "phenomenonTime gt '{}' and phenomenonTime lt '{}'".format(
                download_filter['start'],
                download_filter['end'])
    else:
        middle_frame.forget()

    things_path = os.path.join(project_root, 'things')
    if os.path.exists(things_path):
        shutil.rmtree(things_path)

    os.mkdir(things_path)
    num = 0
    current_url = urls.observations_endpoint(
        base_url) + '?' + urllib.urlencode(params)
    while True:
        # following pagination's next link
        if num * 100 >= max_readings:
            break
        obsv_dict = requests.get(current_url).json()
        if 'value' not in obsv_dict:
            break
        obsv_array = obsv_dict[keys_dict['value']]
        for index, item in enumerate(obsv_array):
            ds_id = item[keys_dict['Datastream']][keys_dict['id']]
            pl.configure(
                text='{} / {}'.format(index + num * 100, max_readings))
            pb['value'] = float(index + num * 100) / max_readings * 100
            if ds_id not in ds_dict:
                ds_dict[ds_id] = item[keys_dict['Datastream']]
                thing_res = requests.get(
                    base_url + 'Datastreams({})?$expand=Thing'.format(
                        ds_id), timeout=timeout).json()['Thing']
                proj_settings = utils.project_setting(project_root)
                if thing_res[keys_dict['id']] not in thing_dict:
                    nt_count += 1
                    new_thing = Thing(thing_res[keys_dict['description']],
                                      item[keys_dict['FeatureOfInterest']],
                                      thing_res[keys_dict['id']],
                                      thing_dir=os.path.join(
                                          project_root, 'things'),
                                      properties=thing_res[
                                          keys_dict['properties']],
                                      proj_setting=proj_settings)
                    thing_dict[thing_res[keys_dict['id']]] = new_thing

                thing_id = thing_res[keys_dict['id']]
                ds_dict[ds_id]['thingId'] = thing_id
                thing_dict[thing_id].add_ds_old(ds_dict[ds_id])
                new_item = {key: item[key] for key in [keys_dict['result'],
                                                       keys_dict[
                                                           'phenomenonTime'],
                                                       keys_dict['id']]}
                thing_dict[thing_id].add_obss(ds_dict[ds_id], new_item)
            else:
                thing_dict[ds_dict[ds_id]['thingId']].add_obss(
                    item[keys_dict['Datastream']], item)
        num += 1
        if keys_dict['nextLink'] in obsv_dict:
            current_url = obsv_dict[keys_dict['nextLink']]
        else:
            break

    proj_settings = utils.project_setting(project_root)
    proj_settings['thingObservations'] = {}

    for key, value in thing_dict.iteritems():
        value.dumps()
        proj_settings['thingObservations'][
            value.thing_id] = thing_dict[key].thing_file_name
        utils.set_project_setting(proj_settings, project_root)
    root.destroy()


def from_datastream(root, url, ds_ids, max_readings, download_filter, debug):
    things_path = os.path.join(project_root, 'things')

    if os.path.exists(things_path):
        try:
            shutil.rmtree(things_path)
        except Exception as e:
            print('Failed to rm things folder, reason %s' % str(e))
            return

    os.mkdir(things_path)
    proj_settings = utils.project_setting(project_root)
    proj_settings['dataStatus'] = utils.DATASTATUS_DOWNLOADING
    proj_settings['thingObservations'] = {}
    utils.set_project_setting(proj_settings, project_root)

    progress_label.pack()
    progress_bar.pack()
    params = {'$expand': 'Observations,Thing'}
    if time_check_var.get() and download_filter.keys():
        params['$filter'] = "phenomenonTime gt '{}' and phenomenonTime lt '{}'".format(
            download_filter['start'],
            download_filter['end'])
    thing_dict = {}
    for index, ds_id in enumerate(ds_ids):
        status_text = r'Loading.. {}/{}'.format(index + 1, len(ds_ids))
        progress_bar['value'] = float(index + 1) / len(ds_ids) * 100
        progress_label.configure(text=status_text)
        ds_url = urls.datastream_endpoint(url, ds_id)
        ds_resp_raw = requests.get(ds_url + '?' + urllib.urlencode(params))
        # TODO: we are assuming response is JSON
        ds_resp_dict = ds_resp_raw.json()
        # TODO: Check if response says broken
        if 'Thing' in ds_resp_dict:
            thing_id = ds_resp_dict[keys_dict['id']]
            if thing_id not in thing_dict:
                # We need to reach the Thing endpoint instead of using expand Thing
                # is that location is not avaialble in the expand
                thing_params = {'$expand': 'Locations'}
                thing_url = urls.thing_endpoint(
                    url, ds_resp_dict['Thing'][keys_dict['id']])
                thing_resp_raw = requests.get(
                    thing_url + '?' + urllib.urlencode(thing_params))
                thing_resp_dict = thing_resp_raw.json()
                new_thing = Thing(thing_resp_dict[keys_dict['description']],
                                  thing_resp_dict['Locations'][0],
                                  thing_resp_dict[keys_dict['id']],
                                  thing_dir=os.path.join(
                                      project_root, 'things'),
                                  properties=thing_resp_dict['properties'],
                                  proj_setting=proj_settings)
                thing_dict[thing_id] = new_thing
            thing_dict[thing_id].add_ds_with_obsv(ds_resp_dict)

    for key, value in thing_dict.iteritems():
        value.dumps()
        proj_settings['thingObservations'][
            value.thing_id] = thing_dict[key].thing_file_name
        utils.set_project_setting(proj_settings, project_root)

    root.destroy()

if __name__ == '__main__':

    def time_check_cmd():
        if time_check_var.get():
            for child in time_entries:
                child.configure(state='enable')
        else:
            for child in time_entries:
                child.configure(state='disable')

    proj_settings = utils.project_setting(project_root)
    parser = argparse.ArgumentParser('download data')
    max_readings = proj_settings['maxReadings']
    if sys.argv[1] in ('test'):
        base_url = proj_settings['defaultUrl']
        spatial_query = "st_within(location, geography 'POLYGON ((-180.0000 75.4299, 13.1086 75.4299, 13.1086 34.7054, -180.0000 34.7054, -180.0000 75.4299)) ')"
        advanced = True
        datastreams = '29741,14314'

    else:
        """
        Arguments
        Positional arguments 
        url: Base Url of sensor servers
        OR
        test: start testing mode if used as first argument

        Optional arguments
        -d --datastream datastream ids
        -a --advanced show GUIs to enble advanced download
        -s --spatialquery spatial query string to use when downloading observations
        -t --timequery time query string to use when downloading observations
        """
        parser.add_argument('url', default='default')

        parser.add_argument('-d', action='store', default='',
                            dest='datastreams', help='datastream ids')
        parser.add_argument('-a', '--advanced', action='store_true',
                            dest='advanced', default=False,
                            help='Show advanced user interface to enable advanced download options')
        parser.add_argument('-s', action='store', dest='spatial_query',
                            type=str, default='', help='spatial query string')
        parser.add_argument('-t', action='store', dest='time_query',
                            type=str, default='', help='temporal query string')

        args = parser.parse_args()
        advanced = args.advanced
        base_url = args.url
        datastreams = args.datastreams
        spatial_query = args.spatial_query
        time_query = args.time_query

    timeout = proj_settings['timeout']

    debug = False
    root = tk.Tk()
    root.wm_title('Download')

    label_row = 1
    entry_row = 2

    # Current view should be loaded from the config file
    filter_options = [('Filter By Time', 'T'), ('Using Current View', 'V')]

    radio_val = tk.StringVar()
    if advanced:
        radio_val.set('T')
    else:
        radio_val.set('')

    def radio_cmd_old():
        if radio_val.get() == 'T':
            for child in time_frame.winfo_children():
                if isinstance(child, ttk.Entry):
                    child.configure(state='enable')

        elif radio_val.get() == 'V':
            for child in time_frame.winfo_children():
                if isinstance(child, ttk.Entry):
                    child.configure(state='disable')

            for child in lower_frame.winfo_children():
                if isinstance(child, ttk.Entry):
                    child.configure(state='enable')

    def radio_cmd():
        if radio_val.get() == 'T':
            for child in ds_entries:
                child.configure(state='enable')
            if not time_check_var.get():
                for child in time_entries:
                    child.configure(state='disable')

        elif radio_val.get() == 'V':
            for child in ds_entries:
                child.configure(state='disable')

    upper_radio = ttk.Radiobutton(root, text=filter_options[0][0], variable=radio_val,
                                  value=filter_options[0][1], command=radio_cmd)

    lower_radio = ttk.Radiobutton(root, text=filter_options[1][0], variable=radio_val,
                                  value=filter_options[1][1], command=radio_cmd)

    # TODO: I should use
    if spatial_query == '':
        lower_radio.configure(state='disable')
    ds_frame = ttk.Frame(root, borderwidth=10, relief=tk.GROOVE)
    if advanced:
        upper_radio.pack(anchor=tk.W, fill=tk.X, expand=tk.YES)
        ds_frame.pack(padx=5, pady=5, anchor=tk.W)

    ds_entries = []
    time_entries = []
    ds_upper_frame = ttk.Frame(ds_frame, relief=tk.GROOVE)
    ds_upper_frame.pack(anchor=tk.W, fill='x')

    ds_label = ttk.Label(ds_upper_frame, text='Data Stream id')
    ds_label.pack(side=tk.LEFT, padx=5, pady=5)
    # ds_label.grid(column=0, row=0, padx=5, pady=5)
    ds_var = tk.StringVar()
    ds_entry = ttk.Entry(ds_upper_frame, textvariable=ds_var)
    ds_var.set(datastreams)
    ds_entry.pack(
        side=tk.LEFT, padx=5, pady=5, anchor=tk.E, fill='x', expand=tk.YES)
    # ds_entry.grid(column=1, row=0, padx=5, pady=5, sticky=tk.E)

    time_check_var = tk.IntVar()
    time_check_button = ttk.Checkbutton(ds_frame, text='Filter by time',
                                        variable=time_check_var, command=time_check_cmd)
    time_check_button.pack(anchor=tk.W)
    time_frame = ttk.Frame(ds_frame, relief=tk.GROOVE)
    time_frame.pack()

    to_default = datetime.now()
    from_default = to_default - timedelta(days=30)

    from_time_label = ttk.Label(time_frame, text='From Time')
    from_time_label.grid(row=0, column=0, columnspan=3, padx=5, pady=5)

    from_year_label = ttk.Label(time_frame, text='YYYY')
    from_year_label.grid(row=label_row, column=0, padx=5, pady=5)
    from_year_entry = ttk.Entry(time_frame, width=4)
    from_year_entry.grid(row=entry_row, column=0, padx=5, pady=5)
    from_year_entry.insert(0, '%04d' % from_default.year)
    time_entries.append(from_year_entry)

    from_month_label = ttk.Label(time_frame, text='MM')
    from_month_label.grid(row=label_row, column=1, padx=5, pady=5)
    from_month_entry = ttk.Entry(time_frame, width=2)
    from_month_entry.grid(row=entry_row, column=1, padx=5, pady=5)
    from_month_entry.insert(0, '%02d' % from_default.month)
    time_entries.append(from_month_entry)

    from_day_label = ttk.Label(time_frame, text='DD')
    from_day_label.grid(row=label_row, column=2, padx=5, pady=5)
    from_day_entry = ttk.Entry(time_frame, width=2)
    from_day_entry.grid(row=entry_row, column=2, padx=5, pady=5)
    from_day_entry.insert(0, '%02d' % from_default.day)
    time_entries.append(from_day_entry)

    from_hr_label = ttk.Label(time_frame, text='hh')
    from_hr_label.grid(row=label_row, column=3, padx=5, pady=5)
    from_hr_entry = ttk.Entry(time_frame, width=2)
    from_hr_entry.grid(row=entry_row, column=3, padx=5, pady=5)
    from_hr_entry.insert(0, '00')
    time_entries.append(from_hr_entry)

    from_min_label = ttk.Label(time_frame, text='mm')
    from_min_label.grid(row=label_row, column=4, padx=5, pady=5)
    from_min_entry = ttk.Entry(time_frame, width=2)
    from_min_entry.grid(row=entry_row, column=4, padx=5, pady=5)
    from_min_entry.insert(0, '00')
    time_entries.append(from_min_entry)

    upper_seperator = ttk.Label(time_frame, text='-')
    upper_seperator.grid(row=1, column=5, padx=5, pady=5)

    lower_seperator = ttk.Label(time_frame, text='-')
    lower_seperator.grid(row=2, column=5, padx=5, pady=5)

    to_column_start = 6
    to_time_label = ttk.Label(time_frame, text='To Time')
    to_time_label.grid(row=0, column=to_column_start, columnspan=3)

    to_year_label = ttk.Label(time_frame, text='YYYY')
    to_year_label.grid(
        row=label_row, column=to_column_start + 1, padx=5, pady=5)
    to_year_entry = ttk.Entry(time_frame, width=4)
    to_year_entry.grid(
        row=entry_row, column=to_column_start + 1, padx=5, pady=5)
    to_year_entry.insert(0, '%04d' % to_default.year)
    time_entries.append(to_year_entry)

    to_month_label = ttk.Label(time_frame, text='MM')
    to_month_label.grid(
        row=label_row, column=to_column_start + 2, padx=5, pady=5)
    to_month_entry = ttk.Entry(time_frame, width=2)
    to_month_entry.grid(
        row=entry_row, column=to_column_start + 2, padx=5, pady=5)
    to_month_entry.insert(0, '%02d' % to_default.month)
    time_entries.append(to_month_entry)

    to_day_label = ttk.Label(time_frame, text='DD')
    to_day_label.grid(
        row=label_row, column=to_column_start + 3, padx=5, pady=5)
    to_day_entry = ttk.Entry(time_frame, width=2)
    to_day_entry.grid(
        row=entry_row, column=to_column_start + 3, padx=5, pady=5)
    to_day_entry.insert(0, '%02d' % to_default.day)
    time_entries.append(to_day_entry)

    to_hr_label = ttk.Label(time_frame, text='hh')
    to_hr_label.grid(row=label_row, column=to_column_start + 4, padx=5, pady=5)
    to_hr_entry = ttk.Entry(time_frame, width=2)
    to_hr_entry.grid(row=entry_row, column=to_column_start + 4, padx=5, pady=5)
    to_hr_entry.insert(0, '00')
    time_entries.append(to_hr_entry)

    to_min_label = ttk.Label(time_frame, text='mm')
    to_min_label.grid(
        row=label_row, column=to_column_start + 5, padx=5, pady=5)
    to_min_entry = ttk.Entry(time_frame, width=2)
    to_min_entry.grid(
        row=entry_row, column=to_column_start + 5, padx=5, pady=5)
    to_min_entry.insert(0, '00')
    time_entries.append(to_min_entry)
    ds_entries = time_entries[:]
    ds_entries.append(ds_entry)
    ds_entries.append(time_check_button)
    for child in time_entries:
        child.configure(state='disable')

    middle_frame = ttk.Frame(root)
    middle_frame.pack()

    num_label = ttk.Label(middle_frame, text='Number of readings')

    num_entry = gui.IntegerEntry(middle_frame, max_readings, width=5)

    if advanced:
        lower_radio.pack(anchor=tk.W)
        num_label.grid(row=0, column=0, padx=5, pady=5)
        num_entry.grid(row=0, column=1, padx=5, pady=5)
    lower_frame = ttk.Frame(root)
    lower_frame.pack()
    progress_label = ttk.Label(lower_frame)

    progress_bar = ttk.Progressbar(lower_frame, mode='determinate')

    status_frame = ttk.Frame(root)
    status_frame.pack()

    button_frame = ttk.Frame(root)
    button_frame.pack()

    def download(advanced=False):
        ''''
        The action for doing the actualy downloading
        '''

        # validatation
        if not radio_val.get() == 'V':
            ds_ids = ds_entry.get().split(',')
            for ds_id in ds_ids:
                try:
                    ds_id_int = int(ds_id)
                except:
                    status_label.configure(text='Datastream Id format wrong, '
                                           'Format:ID1,ID2,ID3',
                                           fg='red')
                    return

        status_label.configure(text='')
        download_filter = {}
        if spatial_query:
            download_filter['view'] = spatial_query
        num_readings = int(num_entry.get())

        if download_filter.keys():
            if radio_val.get() == 'T':
                start_time = datetime(int(from_year_entry.get()),
                                      int(from_month_entry.get()),
                                      int(from_day_entry.get()),
                                      int(from_hr_entry.get()),
                                      int(from_min_entry.get()))

                end_time = datetime(int(to_year_entry.get()),
                                    int(to_month_entry.get()),
                                    int(to_day_entry.get()),
                                    int(to_hr_entry.get()),
                                    int(to_min_entry.get()))

                # TODO: extract this dude in external function
                download_filter = {
                    'start': start_time.isoformat() + 'Z',
                    'end': end_time.isoformat() + 'Z',

                }
                ds_ids_str = ds_entry.get()

                # do validation here
                ds_ids = [int(ds) for ds in ds_ids_str.split(',')]
                t = threading.Thread(target=from_datastream,
                                     args=(root, base_url,
                                           ds_ids, num_readings,
                                           download_filter, debug))
                t.start()
            elif radio_val.get() == 'V':

                t = threading.Thread(target=from_location,
                                     args=(root, base_url, num_readings,
                                           spatial_query, debug))
                t.start()
            else:
                pass
        else:
            ds_ids = [int(ds) for ds in datastreams.split(',')]
            t = threading.Thread(target=from_datastream,
                                 args=(root, base_url,
                                       ds_ids, num_readings,
                                       download_filter, debug))
            t.start()
    status_label = tk.Label(status_frame, text='')
    status_label.pack()
    create_button = ttk.Button(button_frame, text='Download',
                               command=partial(download, True))

    if advanced:
        create_button.pack()
        root.mainloop()
    else:
        download()
        root.mainloop()
