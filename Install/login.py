import requests
import stutils
import os
import threading
import sched, time

cwd = os.path.dirname(os.path.abspath(__file__))
if os.environ.has_key('project_root'):
    project_root = os.environ['project_root']
else:
    # not run with arcgis
    project_root = os.getcwd()


def LogIn():
    import Tkinter
    # ---Window---#
    # make window
    window = Tkinter.Tk()
    # change title
    window.title("Python Games Login")
    # change size
    window.geometry("270x210")
    # change window icon
    # window.wm_iconbitmap("Login icon.ico")
    # change window colour
    window.configure(bg="#39d972")

    def login_request(url, data, msg_label):
        res = requests.post(url, data)
        if res.json()['status'] == u'success':
            msg_label.configure(text="Logged in.")
            print('User logged in')

            proj_settings = stutils.project_setting(project_root)
            proj_settings['status'] = stutils.STATUS_LOGGEDIN
            proj_settings['username'] = res.json()['username']

            stutils.set_project_setting(proj_settings, project_root)

        else:
            msg_label.configure(text="API key does not exist")

        def close_window():
            window.destroy()

        s = sched.scheduler(time.time, time.sleep)
        # not gonna work with lambda
        s.enter(3, 1, close_window, ())
        s.run()

    def login_pressed():
        api_key = key_textfield.get()
        auth_data = {'key': api_key}

        threading.Thread(target=login_request, args=(stutils.urls.auth_url,
                                                     auth_data,
                                                     message)).start()

        # res = requests.post(stutils.urls.auth_url, data=auth_data)
        # if res.json()['status'] == u'success':
        #     message.configure(text="Logged in.")
        #     print('User logged in')

        #     proj_settings = stutils.project_setting(project_root)
        #     proj_settings['status'] = stutils.STATUS_LOGGEDIN
        #     proj_settings['username'] = res.json()['username']

        #     stutils.set_project_setting(proj_settings, project_root)

        # else:
        #     message.configure(text="API key does not exist")
        #     # labels

    login_header = Tkinter.Label(window, text="--Geosens login window--\n",
                                 bg="#39d972")
    api_title = Tkinter.Label(window, text="Please enter your api key",
                              bg="#39d972")
    message = Tkinter.Label(window, bg="#39d972")

    # text entry windows
    var = Tkinter.StringVar()
    key_textfield = Tkinter.Entry(window, textvariable=var)
    var.set('APIKEY')

    # buttons
    go = Tkinter.Button(window, text="Log in!", command=login_pressed,
                        bg="#93ff00")

    # pack widgets
    login_header.pack()
    api_title.pack()
    key_textfield.pack()
    go.pack()
    message.pack()

    # start window
    window.mainloop()

if __name__ == '__main__':
    LogIn()
